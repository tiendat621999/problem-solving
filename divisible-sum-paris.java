đề bài: https://www.hackerrank.com/challenges/divisible-sum-pairs/problem?isFullScreen=true

import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.*;
import java.util.regex.*;
import java.util.stream.*;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;



public class Solution {
    public static void main(String[] args) throws IOException {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int k = in.nextInt();
        int a[] = new int[n];
        for(int a_i=0; a_i < n; a_i++){
            a[a_i] = in.nextInt();
        }
        int cnt=0;
        for (int i = 0 ;i<n-1;i++) {
            for (int j=i+1;j<n;j++) {
                if ((a[i]+a[j]) % k == 0)
                    cnt ++;
            }
        }
        System.out.println(cnt);
    }
}
