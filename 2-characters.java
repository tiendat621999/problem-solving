đề bài: https://www.hackerrank.com/challenges/two-characters/problem?isFullScreen=true

Homecoding problemsHackerRank Two Characters problem solution
HackerRank Two Characters problem solution
YASH PALApril 19, 2021

In this HackerRank Two Characters problem, Given a string, remove characters until the string is made up of any two alternating characters. When you choose a character to remove, all instances of that character must be removed. Determine the longest string possible that contains just two alternating letters.

HackerRank Two Characters problem solution


Problem solution in Python programming.
#!/bin/python3

import sys

def valid(s):
    if len(s) <= 1:
        return False
    if s[0] == s[1]:
        return False
    if len(set(s)) > 2:
        return False
    for i in range(2, len(s)):
        if i%2 == 0:
            if s[i] != s[0]:
                return False
        else:
            if s[i] != s[1]:
                return False
    return True
    

n = int(input().strip())
s = input().strip()

r = 0
alp = 'abcdefghijklmnopqrstuvwxyz'
for k in alp:
    for l in alp:
        if k >= l:
            continue
        f = list(filter(lambda x: x == k or x == l, s))
        if valid(f):
            r = max(r, len(f))
print(r)


Problem solution in Java Programming.
import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;
import java.util.stream.*;

public class Solution {
    static boolean isAlt(int[] str){
        return str[0]!=str[1] && IntStream.range(2,str.length).allMatch(i->str[i]==str[i%2]);
    }
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int len = in.nextInt();
        String s = in.next();
        int[] nums=s.chars().distinct().toArray();
        int maxLength=0;
        for(int i=0; i<nums.length; i++){
            for(int j=i+1; j<nums.length; j++){
                int a=nums[i]; int b=nums[j];
                int [] removed=s.chars().filter(c->c==a||c==b).toArray();
                if(isAlt(removed) && removed.length>maxLength)
                    maxLength=removed.length;
            }
        }
        System.out.println(maxLength);
            
    }
}