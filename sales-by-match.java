đề bài: https://www.hackerrank.com/challenges/sock-merchant/problem?isFullScreen=true

import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.*;
import java.util.regex.*;
import java.util.stream.*;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;



public class Solution {
    public static void main(String[] args) throws IOException {
        Scanner s = new Scanner(System.in);
        int[] freq = new int[101];
        int n = s.nextInt();
        for(int i = 0; i < n; i++){
            int x = s.nextInt();
            freq[x]++;
        }
        int total = 0;
        for(int i = 1; i < 101; i++){
            total+=freq[i]/2;
        }
        System.out.println(total);
    }
}
